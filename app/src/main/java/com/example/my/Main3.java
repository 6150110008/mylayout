package com.example.my;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

public class Main3 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);
    }

    public void linkWeb(View v){
        String myUriString = "https://pantip.com/";
        Intent myActivity2 = new Intent(Intent.ACTION_VIEW,
                Uri.parse(myUriString));
        startActivity(myActivity2);

    }
    public void back1(View v){
        Intent intent = new Intent(Main3.this,Main2.class);
        startActivity(intent);
    }
    public void home (View v){
        Intent intent = new Intent(Main3.this,MainActivity.class);
        startActivity(intent);
    }
}